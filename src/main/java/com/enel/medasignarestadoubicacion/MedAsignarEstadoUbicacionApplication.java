package com.enel.medasignarestadoubicacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.medasignarestadoubicacion.service.FileProcessor;
import com.enel.medasignarestadoubicacion.service.LogProcessor;
import com.enel.medasignarestadoubicacion.service.RegProcessor;
import com.enel.medasignarestadoubicacion.service.UserValidator;

import lombok.extern.slf4j.Slf4j;

import static com.enel.medasignarestadoubicacion.util.Constants.*;

@SpringBootApplication
@Slf4j
public class MedAsignarEstadoUbicacionApplication implements CommandLineRunner {

	@Autowired
	private FileProcessor fileProcessor;
	@Autowired
	private RegProcessor regProcessor;
	@Autowired
	private LogProcessor logProcessor;
	@Autowired
	private UserValidator userValidator;
	
	public static void main(String[] args) {
		SpringApplication.run(MedAsignarEstadoUbicacionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (args.length != NUM_INPUT_PARAMETERS) {
			log.error("Numero de parametros de entrada invalidos");
			System.exit(FAILED_EXECUTION);
		}
		
		log.info("Iniciando proceso...");
		
		String inputFilePath = args[1] + args[2];
		String nroOdt = args[3];
		
		fileProcessor.processFiles(inputFilePath, nroOdt);
		
		userValidator.getUserData(args[4]);
		
		logProcessor.writeLog(TITLE);
		logProcessor.writeLog(
				"Fecha y hora de inicio:\t"
				.concat(logProcessor.getDateTimeSufix(IN_FILE_FORMAT))
				.concat("\n"));
		
		regProcessor.processRegisters(fileProcessor.getRegisters());
		
		log.info("Ejecucion finalizada");
		
		logProcessor.writeLog(
				"\nFecha y hora de fin:\t"
				.concat(logProcessor.getDateTimeSufix(IN_FILE_FORMAT)));
		
		System.exit(SUCCESSFUL_EXECUTION);
	}

}
