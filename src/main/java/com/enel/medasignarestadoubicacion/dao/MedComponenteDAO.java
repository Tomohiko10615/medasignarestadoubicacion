package com.enel.medasignarestadoubicacion.dao;

import com.enel.medasignarestadoubicacion.dto.Componente;
import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;

public interface MedComponenteDAO {

	boolean validarEstadoMedidor(String nombre);

	Componente obtenerDatosComponente(String nombre);

	void actualizarMedComponente(DatosProcesoActualizacion datosProcesoActualizacion);

}
