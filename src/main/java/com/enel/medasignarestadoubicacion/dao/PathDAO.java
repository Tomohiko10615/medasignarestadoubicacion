package com.enel.medasignarestadoubicacion.dao;

public interface PathDAO {

	String getPath(String inputPathKey);

}
