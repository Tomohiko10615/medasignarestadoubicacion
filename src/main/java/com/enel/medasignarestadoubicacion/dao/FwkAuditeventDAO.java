package com.enel.medasignarestadoubicacion.dao;

import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;

public interface FwkAuditeventDAO {

	Long obtenerIdAuditevent();

	void crearNuevaAuditoria(DatosProcesoActualizacion datosProcesoActualizacion);

}
