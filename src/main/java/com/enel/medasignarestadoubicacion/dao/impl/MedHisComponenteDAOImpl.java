package com.enel.medasignarestadoubicacion.dao.impl;

import static com.enel.medasignarestadoubicacion.dao.queries.MedHisComponenteDAOQueries.*;
import static com.enel.medasignarestadoubicacion.util.Constants.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.MedHisComponenteDAO;
import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class MedHisComponenteDAOImpl implements MedHisComponenteDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void actualizarMedHisComponente(Long idHisComponente) {
		String sqlScript = UPDATE_MED_HIS_COMPONENTE;
		try {
			jdbcTemplate.update(sqlScript, idHisComponente);
		} catch (Exception e) {
			log.error("No se pudo actualizar medHisComponente con idHisComponente : {}", idHisComponente);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
	}

	@Override
	public void crearNuevoMedHisComponente(DatosProcesoActualizacion datosProcesoActualizacion) {
		String sqlScript = INSERT_INTO_MED_HIS_COMPONENTE;
		try {
			jdbcTemplate.update(sqlScript,
					datosProcesoActualizacion.getIdAuditevent(),
					datosProcesoActualizacion.getIdComponente(),
					datosProcesoActualizacion.getEstadoDisponible(),
					datosProcesoActualizacion.getIdAlmacen());
		} catch (Exception e) {
			log.error("No se pudo insertar en medHisComponente con : {}", datosProcesoActualizacion.toString());
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
	}

}
