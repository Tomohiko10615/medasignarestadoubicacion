package com.enel.medasignarestadoubicacion.dao.impl;

import static com.enel.medasignarestadoubicacion.util.Constants.*;
import static com.enel.medasignarestadoubicacion.dao.queries.MedComponenteDAOQueries.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.MedComponenteDAO;
import com.enel.medasignarestadoubicacion.dto.Componente;
import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;
import com.enel.medasignarestadoubicacion.dto.mapper.ComponenteMapper;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class MedComponenteDAOImpl implements MedComponenteDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean validarEstadoMedidor(String nombre) {
		String sqlScript = COUNT_MEDIDORES_CREADOS;
		int countMedidoresCreados = 0;
		try {
			countMedidoresCreados = jdbcTemplate.queryForObject(sqlScript, Integer.class, nombre);
		} catch (Exception e) {
			log.error("No se pudo validar el estado del medidor {}", nombre);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return countMedidoresCreados == 1;
	}

	@Override
	public Componente obtenerDatosComponente(String nombre) {
		String sqlScript = SELECT_FOR_DATOS_COMPONENTE;
		Componente componente = null;
		try {
			componente = jdbcTemplate.queryForObject(sqlScript, new ComponenteMapper(), nombre);
		} catch (Exception e) {
			log.error("No se pudo obtener los datos del componente : {}", nombre);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return componente;
	}

	@Override
	public void actualizarMedComponente(DatosProcesoActualizacion datosProcesoActualizacion) {
		String sqlScript = UPDATE_MED_COMPONENTE;
		try {
			jdbcTemplate.update(sqlScript,
					datosProcesoActualizacion.getEstadoDisponible(),
					datosProcesoActualizacion.getIdAlmacen(),
					datosProcesoActualizacion.getIdComponente());
		} catch (Exception e) {
			log.error("No se pudo actualizar medComponente con : {}", datosProcesoActualizacion.toString());
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
	}

}
