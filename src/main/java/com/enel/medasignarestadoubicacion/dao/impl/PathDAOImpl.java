package com.enel.medasignarestadoubicacion.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.PathDAO;

import lombok.extern.slf4j.Slf4j;

import static com.enel.medasignarestadoubicacion.dao.queries.PathDAOQueries.*;
import static com.enel.medasignarestadoubicacion.util.Constants.*;

@Repository
@Slf4j
public class PathDAOImpl implements PathDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Override
	public String getPath(String pathKey) {
		String sqlScript = SELECT_FOR_PATH;
		String path = null;
		try {
			path = jdbcTemplate.queryForObject(sqlScript, String.class, pathKey);
		} catch (Exception e) {
			log.error("No se pudo obtener el path para : {}", pathKey);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return path;
	}
	
}
