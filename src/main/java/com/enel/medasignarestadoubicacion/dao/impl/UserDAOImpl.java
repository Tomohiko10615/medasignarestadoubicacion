package com.enel.medasignarestadoubicacion.dao.impl;

import static com.enel.medasignarestadoubicacion.util.Constants.FAILED_EXECUTION;
import static com.enel.medasignarestadoubicacion.dao.queries.UserDAOQueries.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.UserDAO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDAOImpl implements UserDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Long obtenerIdUsuario(String username) {
		String sqlScript = SELECT_FOR_ID_USUARIO;
		Long idUsuario = null;
		try {
			idUsuario = jdbcTemplate.queryForObject(sqlScript, Long.class, username);
		} catch (Exception e) {
			log.error("No se pudo obtener el idUsuario para : {}", username);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return idUsuario;
	}

}
