package com.enel.medasignarestadoubicacion.dao.impl;

import static com.enel.medasignarestadoubicacion.dao.queries.MedEquivSapDaoQueries.*;
import static com.enel.medasignarestadoubicacion.util.Constants.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.MedEquivSapDAO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class MedEquivSapDaoImpl implements MedEquivSapDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean validarExistenciaEnAlmacen(String valor) {
		String sqlScript = COUNT_MEDIDORES_EN_ALMACEN;
		int countMedidoresEnAlmacen = 0;
		try {
			countMedidoresEnAlmacen = jdbcTemplate.queryForObject(sqlScript, Integer.class, valor);
		} catch (Exception e) {
			log.error("No se pudo validar la existencia del medidor en almacen {}", valor);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return countMedidoresEnAlmacen == 1;
	}

	@Override
	public Long obtenerIdAlmacen(String valor) {
		String sqlScript = SELECT_FOR_ID_ALMACEN;
		Long idAlmacen = null;
		try {
			String codSynergia = jdbcTemplate.queryForObject(sqlScript, String.class, valor);
			idAlmacen = Long.parseLong(codSynergia);
		} catch (Exception e) {
			log.error("No se pudo obtener el idAlmacen : {}", valor);
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return idAlmacen;
	}

}
