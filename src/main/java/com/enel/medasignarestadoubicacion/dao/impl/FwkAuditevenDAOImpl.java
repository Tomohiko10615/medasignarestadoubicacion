package com.enel.medasignarestadoubicacion.dao.impl;

import static com.enel.medasignarestadoubicacion.dao.queries.FwkAuditeventDAOQueries.*;
import static com.enel.medasignarestadoubicacion.util.Constants.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.enel.medasignarestadoubicacion.dao.FwkAuditeventDAO;
import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class FwkAuditevenDAOImpl implements FwkAuditeventDAO {

	@Autowired
	@Qualifier("postgresJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public Long obtenerIdAuditevent() {
		String sqlScript = SELECT_FOR_ID_AUDITEVENT;
		Long idAuditevent = null;
		try {
			idAuditevent = jdbcTemplate.queryForObject(sqlScript, Long.class);
		} catch (Exception e) {
			log.error("No se pudo obtener el idAuditevent");
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
		return idAuditevent;
	}

	@Override
	public void crearNuevaAuditoria(DatosProcesoActualizacion datosProcesoActualizacion) {
		String sqlScript = INSERT_INTO_FWK_AUDITEVENT;
		try {
			jdbcTemplate.update(sqlScript,
					datosProcesoActualizacion.getIdAuditevent(),
					datosProcesoActualizacion.getIdComponente(),
					datosProcesoActualizacion.getIdUsuario());
		} catch (Exception e) {
			log.error("No se pudo insertar en fwkAuditevent con : {}", datosProcesoActualizacion.toString());
			log.error("{}", e.getMessage());
			System.exit(FAILED_EXECUTION);
		}
	}

}
