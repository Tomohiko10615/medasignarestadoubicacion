package com.enel.medasignarestadoubicacion.dao.queries;

public class PathDAOQueries {
	
	private PathDAOQueries() {
		super();
	}

	public static final String SELECT_FOR_PATH = "SELECT path "
			+ "FROM COM_PATH "
			+ "WHERE key = ?";
}
