package com.enel.medasignarestadoubicacion.dao.queries;

public class MedComponenteDAOQueries {
	
	private MedComponenteDAOQueries() {
		super();
	}
	
	public static final String COUNT_MEDIDORES_CREADOS =
			"        SELECT COUNT(*) "
			+ "        FROM MED_COMPONENTE MC, MED_BARCODE MB\n"
			+ "        WHERE MC.ID = MB.ID_COMPONENTE\n"
			+ "            AND MB.COD_MEDIDOR = ?\n"
			+ "            AND MC.ID_EST_COMPONENTE =16";
	
	public static final String SELECT_FOR_DATOS_COMPONENTE = "SELECT MC.ID as id_componente, "
			+ " (SELECT MAX(ID_HIS_COMPONENTE) FROM MED_HIS_COMPONENTE WHERE ID_COMPONENTE = MC.ID) as id_his_componente\n"
			+ "                FROM MED_COMPONENTE MC, MED_BARCODE MB\n"
			+ "                WHERE MC.ID = MB.ID_COMPONENTE\n"
			+ "                    AND MB.COD_MEDIDOR = ?\n"
			+ "                    AND MC.ID_EST_COMPONENTE =16";
	
	public static final String UPDATE_MED_COMPONENTE = "UPDATE MED_COMPONENTE SET ID_EST_COMPONENTE = ?, ID_UBICACION = ?, TYPE_UBICACION = 'com.synapsis.synergia.med.domain.componente.Almacen'\n"
			+ "                    WHERE ID = ?";

}
