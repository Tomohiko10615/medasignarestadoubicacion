package com.enel.medasignarestadoubicacion.dao.queries;

public class MedEquivSapDaoQueries {
	
	private MedEquivSapDaoQueries() {
		super();
	}

	public static final String COUNT_MEDIDORES_EN_ALMACEN =
			"SELECT COUNT(*) \n"
			+ "            FROM MED_EQUIV_SAP \n"
			+ "            WHERE SISTEMA = 'TRAZABILIDADMEDSYN' \n"
			+ "                AND TIPO_CODIGO = 'UBICACION'\n"
			+ "                AND COD_SAP = ?";
	
	public static final String SELECT_FOR_ID_ALMACEN = "SELECT COD_SYNERGIA \n"
			+ "                FROM MED_EQUIV_SAP \n"
			+ "                WHERE SISTEMA = 'TRAZABILIDADMEDSYN' \n"
			+ "                    AND TIPO_CODIGO = 'UBICACION'\n"
			+ "                    AND COD_SAP = ?";
}
