package com.enel.medasignarestadoubicacion.dao.queries;

public class MedHisComponenteDAOQueries {

	private MedHisComponenteDAOQueries() {
		super();
	}
	
	public static final String UPDATE_MED_HIS_COMPONENTE =
			"UPDATE MED_HIS_COMPONENTE SET FEC_HASTA = NOW() "
			+ "WHERE ID_HIS_COMPONENTE = ?";
	
	public static final String INSERT_INTO_MED_HIS_COMPONENTE = "INSERT INTO MED_HIS_COMPONENTE (ID_HIS_COMPONENTE, ID_COMPONENTE, ID_EST_COMPONENTE, FEC_DESDE, FEC_HASTA, ID_UBICACION, TYPE_UBICACION, ID_ORDEN)\n"
			+ "                    VALUES (?, ?, ?, now(), NULL, ?, 'com.synapsis.synergia.med.domain.componente.Almacen', NULL)";

}
