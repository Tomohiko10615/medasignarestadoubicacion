package com.enel.medasignarestadoubicacion.dao.queries;

public class FwkAuditeventDAOQueries {
	
	private FwkAuditeventDAOQueries() {
		super();
	}

	public static final String SELECT_FOR_ID_AUDITEVENT = "SELECT NEXTVAL('SQAUDITEVENT')";
	
	public static final String INSERT_INTO_FWK_AUDITEVENT = "INSERT INTO FWK_AUDITEVENT (ID, USECASE, OBJECTREF, ID_FK, FECHA_EJECUCION, SPECIFIC_AUDITEVENT, ID_USER)\n"
			+ "                    VALUES (?, 'Medidor.UPDATE', 'com.synapsis.synergia.med.domain.componente.Medidor', ?, NOW(), 'COMPONENTE', ?)";
	
}
