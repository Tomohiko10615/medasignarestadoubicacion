package com.enel.medasignarestadoubicacion.dao;

public interface MedEquivSapDAO {

	boolean validarExistenciaEnAlmacen(String valor);

	Long obtenerIdAlmacen(String valor);

}
