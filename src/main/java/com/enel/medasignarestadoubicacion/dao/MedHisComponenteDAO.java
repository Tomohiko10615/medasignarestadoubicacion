package com.enel.medasignarestadoubicacion.dao;

import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;

public interface MedHisComponenteDAO {

	void actualizarMedHisComponente(Long idHisComponente);

	void crearNuevoMedHisComponente(DatosProcesoActualizacion datosProcesoActualizacion);

}
