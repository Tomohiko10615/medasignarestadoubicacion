package com.enel.medasignarestadoubicacion.dao;

public interface UserDAO {

	Long obtenerIdUsuario(String username);

}
