package com.enel.medasignarestadoubicacion.dto.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.enel.medasignarestadoubicacion.dto.Componente;

public class ComponenteMapper implements RowMapper<Componente> {

	@Override
	public Componente mapRow(ResultSet rs, int rowNum) throws SQLException {
		return Componente.builder()
				.idComponente(rs.getLong("id_componente"))
				.idHisComponente(rs.getLong("id_his_componente"))
				.build();
	}

}
