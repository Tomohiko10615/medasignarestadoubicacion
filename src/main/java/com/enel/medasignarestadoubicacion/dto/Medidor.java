package com.enel.medasignarestadoubicacion.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Medidor {

	private Long idLost;
	private Long idOrdVenta;
	private String nombre;
	private String valor;
	private String line;
}
