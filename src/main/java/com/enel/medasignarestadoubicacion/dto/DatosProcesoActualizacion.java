package com.enel.medasignarestadoubicacion.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class DatosProcesoActualizacion {

	private Long idAlmacen;
	private Long idComponente;
	private Long idHisComponente;
	private Long idAuditevent;
	private int estadoDisponible;
	private Long idUsuario;
}
