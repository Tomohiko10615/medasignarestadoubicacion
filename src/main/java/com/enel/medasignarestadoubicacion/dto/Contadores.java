package com.enel.medasignarestadoubicacion.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Contadores {

	private Integer procesados = 0;
	private Integer actualizados = 0;
	private Integer invalidos = 0;
	
	public void aumentarInvalidos() {
		invalidos++;
	}

	public void aumentarProcesados() {
		procesados++;
	}

	public void aumentarActualizados() {
		actualizados++;
	}
}
