package com.enel.medasignarestadoubicacion.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Componente {

	private Long idComponente;
	private Long idHisComponente;
}
