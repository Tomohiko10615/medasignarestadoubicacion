package com.enel.medasignarestadoubicacion.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class ScomDataSourceJDBCConfiguration {
	
	@Bean(name = "postgresDatasource") 
	@Primary
	public DataSource postgresDataSource() {
		BeanDataSource beanConn = DataSourceConfig.getConnectionDatos("POST");
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(beanConn.getDbDriver());
        dataSourceBuilder.url(beanConn.getDbURL());
        dataSourceBuilder.username(beanConn.getDbUser());
        dataSourceBuilder.password(beanConn.getDbPass());        
        return dataSourceBuilder.build();
	}
	
	@Bean(name = "postgresConn")
	Connection registrarConnection(@Qualifier("postgresDatasource") DataSource postgresDatasource) throws SQLException {
		return postgresDatasource.getConnection();
	}
	
	@Bean(name = "postgresJdbcTemplate")
	@DependsOn("postgresDatasource")
	public JdbcTemplate jdbcTemplate(@Qualifier("postgresConn") Connection conn) throws SQLException {
		conn.setAutoCommit(false);
		return new JdbcTemplate(new SingleConnectionDataSource(conn, true));
	}
	
}
