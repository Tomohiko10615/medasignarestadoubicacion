package com.enel.medasignarestadoubicacion.service;

import static com.enel.medasignarestadoubicacion.util.Constants.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.medasignarestadoubicacion.dao.FwkAuditeventDAO;
import com.enel.medasignarestadoubicacion.dao.MedComponenteDAO;
import com.enel.medasignarestadoubicacion.dao.MedEquivSapDAO;
import com.enel.medasignarestadoubicacion.dao.MedHisComponenteDAO;
import com.enel.medasignarestadoubicacion.dto.Componente;
import com.enel.medasignarestadoubicacion.dto.Contadores;
import com.enel.medasignarestadoubicacion.dto.DatosProcesoActualizacion;
import com.enel.medasignarestadoubicacion.dto.Medidor;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RegProcessor {
	
	@Autowired
	private Connection conn;

	@Autowired
	private MedComponenteDAO medComponenteDAO;
	@Autowired
	private MedEquivSapDAO medEquivSapDAO;
	@Autowired
	private FwkAuditeventDAO fwkAuditeventDAO;
	@Autowired
	private MedHisComponenteDAO medHisComponenteDAO;
	@Autowired
	private UserValidator userValidator;
	
	@Autowired
	private LogProcessor logProcessor;
	private Medidor registroActual;
	private DatosProcesoActualizacion datosProcesoActualizacion;
	private Contadores contadores = new Contadores();
	
	public void processRegisters(List<Medidor> registers) throws IOException, SQLException {
		for (Medidor reg : registers) {
			registroActual = reg;
			contadores.aumentarProcesados();
			logProcessor.writeLog("\nLínea "
					.concat(contadores.getProcesados().toString())
					.concat(":\t\t")
					.concat(registroActual.getLine())
					.concat("\nResultado:\t\t"));
			if (validarRegistro()) {
				boolean esMedidorValido = validarMedidor();
				if (esMedidorValido) {
					log.info("Medidor {} valido", registroActual.getNombre());
					datosProcesoActualizacion = obtenerDatosParaActualizar();
					log.info("Datos para actualizar: {}", datosProcesoActualizacion.toString());
					actualizarRegistros();
				} else {
					contadores.aumentarInvalidos();
					log.warn("Medidor {} NO valido", registroActual.getNombre());
				}
			}
		}
		logProcessor.writeLog(
				"\nRegistros procesados:\t"
				.concat(contadores.getProcesados().toString())
				.concat("\nRegistros actualizados:\t")
				.concat(contadores.getActualizados().toString())
				.concat("\nRegistros inválidos:\t")
				.concat(contadores.getInvalidos().toString())
				.concat("\n"));
	}

	private boolean validarRegistro() throws IOException {
		if (registroActual.getIdOrdVenta() == null) {
			contadores.aumentarInvalidos();
			log.warn("Registro invalido");
			logProcessor.writeLog("Número de campos incorrecto\n");
			return false;
		}
		if (registroActual.getIdOrdVenta() != VALID_ID_ORD_VENTA) {
			contadores.aumentarInvalidos();
			log.warn("Registro invalido");
			logProcessor.writeLog("ID_ORD_VENTA no válido\n");
			return false;
		}
		return true;
	}

	private DatosProcesoActualizacion obtenerDatosParaActualizar() {
		Componente componente = medComponenteDAO.obtenerDatosComponente(registroActual.getNombre());
		return DatosProcesoActualizacion.builder()
				.idAlmacen(medEquivSapDAO.obtenerIdAlmacen(registroActual.getValor()))
				.idComponente(componente.getIdComponente())
				.idHisComponente(componente.getIdHisComponente())
				.idAuditevent(fwkAuditeventDAO.obtenerIdAuditevent())
				.estadoDisponible(ESTADO_DISPONIBLE)
				.idUsuario(userValidator.getIdUsuario())
				.build();
	}

	private boolean validarMedidor() throws IOException {
		boolean estadoCreado = 
				medComponenteDAO.validarEstadoMedidor(registroActual.getNombre());
		boolean existeEnAlmacen = false;
		if (estadoCreado) {
			existeEnAlmacen = 
					medEquivSapDAO.validarExistenciaEnAlmacen(registroActual.getValor());
			if (!existeEnAlmacen) {
				logProcessor.writeLog("No existe en almacén\n");
			}
		} else {
			logProcessor.writeLog("No está en estado creado\n");
		}
		return estadoCreado && existeEnAlmacen;
	}
	
	private void actualizarRegistros() throws IOException, SQLException {
		log.info("Actualizando registros...");
		medHisComponenteDAO.actualizarMedHisComponente(
				datosProcesoActualizacion.getIdHisComponente());
		medComponenteDAO.actualizarMedComponente(datosProcesoActualizacion);
		medHisComponenteDAO.crearNuevoMedHisComponente(datosProcesoActualizacion);
		fwkAuditeventDAO.crearNuevaAuditoria(datosProcesoActualizacion);
		conn.commit();
		contadores.aumentarActualizados();
		logProcessor.writeLog("Actualizado con éxito\n");
	}
}
