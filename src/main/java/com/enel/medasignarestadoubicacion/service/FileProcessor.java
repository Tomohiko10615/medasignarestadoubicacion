package com.enel.medasignarestadoubicacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.medasignarestadoubicacion.dao.PathDAO;
import com.enel.medasignarestadoubicacion.dto.Medidor;

import lombok.extern.slf4j.Slf4j;

import static com.enel.medasignarestadoubicacion.util.Constants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FileProcessor {
	
	@Autowired
	private PathDAO pathDAO;
	
	@Autowired
	private LogProcessor logProcessor;
	
	private String outputFilePath;
	private List<Medidor> medidores = new ArrayList<>();
	
	public void processFiles(String inputFilePath, String nroOdt) throws IOException {
		loadPaths();
		openInputFile(inputFilePath);
		logProcessor.createOutputFile(outputFilePath, nroOdt);
	}

	public void loadPaths() {
		outputFilePath = pathDAO.getPath(OUTPUT_PATH_KEY);
		log.info("Se obtuvo el path de salida: {}", outputFilePath);
	}
	
	private void openInputFile(String inputFilePath) throws IOException {
		log.info("Se obtuvo el path de entrada: {}", inputFilePath);
		File inputFile = new File(inputFilePath);
		
		if (inputFile.exists()) {
			readFile(inputFile);
		} else {
			log.error("El archivo de entrada no existe.");
			System.exit(FAILED_EXECUTION);
		}
	}

	private void readFile(File inputFile) throws IOException {
		FileReader reader = new FileReader(inputFile);
		try (BufferedReader archivoEntradaReader = new BufferedReader(reader)) {
			String line = archivoEntradaReader.readLine();
			
			while (line != null) {
				String[] cols = (line).split(SEPARATOR, ENABLE_VOID_FIELDS);
				if (cols.length == NUM_INPUT_FIELDS) {
					medidores.add(Medidor.builder()
							.line(line)
							.idLost(Long.parseLong(cols[0]))
							.idOrdVenta(Long.parseLong(cols[1]))
							.nombre(cols[2])
							.valor(cols[3])
							.build());
				} else {
					medidores.add(Medidor.builder()
							.line(line)
							.build());
				}
				line = archivoEntradaReader.readLine();
			}
		}
	}

	public List<Medidor> getRegisters() {
		return medidores;
	}
	
}
