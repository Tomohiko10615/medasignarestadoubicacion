package com.enel.medasignarestadoubicacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.medasignarestadoubicacion.dao.UserDAO;

@Service
public class UserValidator {
	
	@Autowired
	private UserDAO userDAO;
	
	private Long idUsuario;

	public void getUserData(String username) {
		idUsuario = userDAO.obtenerIdUsuario(username);
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

}
