package com.enel.medasignarestadoubicacion.service;

import static com.enel.medasignarestadoubicacion.util.Constants.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

@Service
public class LogProcessor {
	
	private String logFile;
	
	public void createOutputFile(String outputFilePath, String nroOdt) throws IOException {
		logFile = outputFilePath + OUTPUT_FILE_NAME + nroOdt + ".txt";
		Path logFilePath = Paths.get(logFile);
		Files.createFile(logFilePath);
	}
	
	public void writeLog(String textToWrite) throws IOException {
		try(PrintWriter writer = 
				new PrintWriter(
						new FileWriter(logFile, true))) {
			writer.write(textToWrite);
		}
	}
	
	public String getDateTimeSufix(String format) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);  
		LocalDateTime currentLocalDateTime = LocalDateTime.now();
		return dtf.format(currentLocalDateTime);
	}

}
