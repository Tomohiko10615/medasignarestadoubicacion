package com.enel.medasignarestadoubicacion.util;

public class Constants {

	private Constants() {
		super();
	}

	public static final int NUM_INPUT_PARAMETERS = 5;
	public static final int NUM_INPUT_FIELDS = 4;
	public static final int SUCCESSFUL_EXECUTION = 0;
	public static final int FAILED_EXECUTION = 1;
	public static final int ENABLE_VOID_FIELDS = -1;
	public static final int VALID_ID_ORD_VENTA = 1;
	public static final int ESTADO_DISPONIBLE = 17;
	public static final String INPUT_PATH_KEY = "ORM_REPORTES_IN";
	public static final String OUTPUT_PATH_KEY = "SCOM_ODT_OUTPUT";
	public static final String OUTPUT_FILE_NAME = "MEDCambioEstado_";
	public static final String SLASH = "/";
	public static final String SEPARATOR = "\\|";
	public static final String TITLE = 
			  "#############################################\n"
			+ "##### ODT CAMBIO DE ESTADO DE MEDIDORES #####\n"
			+ "#############################################\n\n";
	public static final String FILE_NAME_FORMAT = "yyyy_MM_dd_HH_mm_ss";
	public static final String IN_FILE_FORMAT = "yyyy/MM/dd HH:mm:ss";
	
}
